# Kind

```sh
kind create cluster
```

Add MetalLB

```sh
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
```

Config MetalLB

```sh
docker network inspect -f '{{.IPAM.Config}}' kind
```

Modify the crd according to the output of the previous command

```sh
kubectl apply -f address-pool.yaml
```

Add gitlab agent

```sh
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install kind gitlab/gitlab-agent \
    --namespace gitlab-agent-kind \
    --create-namespace \
    --set image.tag=v16.11.0-rc2 \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com
```

Deploy argo-cd

```sh
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Get initial password

```sh
argocd admin initial-password -n argocd
```

Port forward ArgoCD

```sh
kubectl port-forward services/argocd-server 8080:80 8443:443 -n argocd
```

## ArgoCD Demo

1. Deploy application

```sh
kubectl apply -f application.yaml
```

2. Port forward traefik

```sh
kubectl port-forward services/core-traefik 9000:9000 8123:80 -n core
```
